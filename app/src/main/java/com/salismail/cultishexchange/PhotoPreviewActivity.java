package com.salismail.cultishexchange;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PhotoPreviewActivity extends AppCompatActivity implements View.OnClickListener {

    private String filePath;
    private Boolean isForNewPost;

    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_preview);

        // change status bar color
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(Color.BLACK);

        prefs = getSharedPreferences("postImages", Context.MODE_PRIVATE);
        editor = prefs.edit();

        ImageView capturedPhoto = (ImageView) findViewById(R.id.capturedPhoto);

        ImageButton closePreview = (ImageButton) findViewById(R.id.closePreview);
        ImageButton ignoreBtn = (ImageButton) findViewById(R.id.retakePhoto);
        ImageButton acceptBtn = (ImageButton) findViewById(R.id.postPhoto);

        closePreview.setOnClickListener(this);
        ignoreBtn.setOnClickListener(this);
        acceptBtn.setOnClickListener(this);

        if (getIntent().hasExtra("imageUri")) {
            filePath = getIntent().getStringExtra("imageUri");
        } else {
            String contentPath = getIntent().getStringExtra("galleryUri");
            Uri contentUri = Uri.parse(contentPath);

            filePath = getRealPathFromURI(getBaseContext(), contentUri);
        }
        isForNewPost = getIntent().getBooleanExtra("isForNewPost", true);

        // Read bitmap from local file
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

        float width = bitmap.getWidth();
        float height = bitmap.getHeight();

        File curFile = new File(Uri.parse(filePath).getPath());

        try {
            ExifInterface exif = new ExifInterface(curFile.getPath());
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int rotationInDegrees = exifToDegrees(rotation);
            if (rotationInDegrees == 90 || rotationInDegrees == 270) {
                width = bitmap.getHeight();
                height = bitmap.getWidth();
            }
        } catch(IOException ex){
            ex.printStackTrace();
        }

        if (prefs.getInt("imageNumber", 0) == 0) {
            editor.putFloat("ratio", height/width);
            editor.commit();
        }

        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        float deviceWidth = size.x;

        int realHeight = Math.round(height * (deviceWidth / width));

        capturedPhoto.getLayoutParams().height = realHeight;

        Glide.with(getBaseContext()).load(new File(Uri.parse(filePath).getPath())).into(capturedPhoto);
    }

    public void onClick(View v) {
        ImageView capturedPhoto = (ImageView) findViewById(R.id.capturedPhoto);
        if (v.getId() == R.id.closePreview || v.getId() == R.id.retakePhoto) {
            Intent intent = new Intent(getBaseContext(), CameraActivity.class);
            String fromActivity = isForNewPost? "home" : "post";
            intent.putExtra("fromActivity", fromActivity);
            startActivity(intent);
        } else if (v.getId() == R.id.postPhoto) {
            String uriSet = prefs.getString("uriset", "");
            int imageNumber = prefs.getInt("imageNumber", 0);
            List<String> uriList;
            if (uriSet.equals("")) {
                uriList = new ArrayList<String>();
            } else {
                uriList = new ArrayList<String>(Arrays.asList(uriSet.split("\\s*,\\s*")));
            }
            String uriUpdatedString = "";
            uriList.add(filePath);
            for (int i = 0; i < uriList.size(); i++) {
                if (i == 0) uriUpdatedString += uriList.get(i);
                else uriUpdatedString+= ("," + uriList.get(i));
            }
            editor.putString("uriset", uriUpdatedString);
            editor.commit();

            Intent intent = new Intent(getBaseContext(), PostActivity.class);
            startActivity(intent);
        }
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) { return 90; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {  return 180; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {  return 270; }
        return 0;
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}