package com.salismail.cultishexchange.utils.qb;

public interface PaginationHistoryListener {
    void downloadMore();
}
