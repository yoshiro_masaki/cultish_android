package com.salismail.cultishexchange.utils.qb;

public interface QbSessionStateCallback {

    void onSessionCreated(boolean success);
}
