package com.salismail.cultishexchange;

import android.content.Context;
import android.media.ExifInterface;
import android.net.Uri;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.File;

/**
 * Created by swordmaster on 5/22/2016.
 */
public class PhotoLayout extends RelativeLayout {

    private String pictureUri;
    private int imageNumber = 0;
    float scale;

    public PhotoLayout(Context context, int width, int height, int marginRight, int marginBottom, int num, String picUri) {
        super(context);
        pictureUri = picUri;
        imageNumber = num;

        GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
        layoutParams.setMargins(0, 0, marginRight, marginBottom);
        layoutParams.width = width;
        layoutParams.height = height;
        this.setLayoutParams(layoutParams);
        this.setBackgroundResource(R.drawable.add_post_image_background);
        addViews(width);
    }

    private void addViews(int width) {
        RoundedImageView imageView = new RoundedImageView(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
        imageView.setLayoutParams(layoutParams);
        scale = getResources().getDisplayMetrics().density;
        imageView.setCornerRadius((int) (6 * scale + 0.5f));
        if (pictureUri.equals(null) || pictureUri.equals("")) {
            imageView.setVisibility(INVISIBLE);
        } else {
            if (pictureUri.split("\\s*:\\s*")[0].equals("http")) {
                Glide.with(getContext()).load(pictureUri).override(width, width).centerCrop().into(imageView);
            } else {
                File curFile = new File(Uri.parse(pictureUri).getPath());
                Glide.with(getContext()).load(curFile).override(width, width).centerCrop().into(imageView);
            }
        }
        ImageButton closeBtn = new ImageButton(getContext());
        closeBtn.setScaleType(ImageButton.ScaleType.FIT_CENTER);
        closeBtn.setImageResource(R.drawable.close);
        closeBtn.setBackgroundResource(android.R.color.transparent);
        closeBtn.setPadding((int) (10 * scale + 0.5f), (int) (10 * scale + 0.5f), (int) (10 * scale + 0.5f), (int) (10 * scale + 0.5f));
        RelativeLayout.LayoutParams closeParams = new RelativeLayout.LayoutParams((int) (40 * scale + 0.5f), (int) (40 * scale + 0.5f));
        closeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        closeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        closeBtn.setLayoutParams(closeParams);
        closeBtn.setTag(pictureUri);
        this.addView(imageView);
        this.addView(closeBtn);
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) { return 90; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {  return 180; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {  return 270; }
        return 0;
    }
}
