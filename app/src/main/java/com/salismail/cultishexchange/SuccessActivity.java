package com.salismail.cultishexchange;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.Image;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class SuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);

        // change status bar color
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(Color.BLACK);

        setupActionBar();

        final Button anotherPost = (Button) findViewById(R.id.postAnotherBtn);
        anotherPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), CameraActivity.class);
                intent.putExtra("fromActivity", "home");
                SharedPreferences prefs = getSharedPreferences("postImages", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("uriset", "");
                editor.putInt("imageNumber", 0);
                editor.commit();
                startActivity(intent);
            }
        });

        final ImageButton shareAppBtn = (ImageButton) findViewById(R.id.rightActionButton);
        shareAppBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(SuccessActivity.this, shareAppBtn);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.select_social, popup.getMenu());

                final PackageManager pm = getPackageManager();

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        int id = item.getItemId();

                        //noinspection SimplifiableIfStatement
                        if (id == R.id.emaill) {
                            Intent intent = new Intent(Intent.ACTION_SEND);

                            intent.setType("message/rfc822");

                            String text = getIntent().getStringExtra("shareText");

                            intent.putExtra(Intent.EXTRA_TEXT, text);

                            startActivity(Intent.createChooser(intent, "Share with"));

                            return true;
                        } else if (id == R.id.whatsapp) {
                            try {
                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.setType("text/plain");

                                String text = getIntent().getStringExtra("shareText");

                                pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                                intent.setPackage("com.whatsapp");

                                intent.putExtra(Intent.EXTRA_TEXT, text);
                                startActivity(Intent.createChooser(intent, "Share with"));

                            } catch (PackageManager.NameNotFoundException e) {
                                Toast.makeText(SuccessActivity.this, R.string.package_whatsapp_error, Toast.LENGTH_SHORT).show();
                            }

                            return true;
                        } else if (id == R.id.wechat) {
                            try {
                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.setType("text/plain");

                                String text = getIntent().getStringExtra("shareText");

                                pm.getPackageInfo("com.tencent.mm", PackageManager.GET_META_DATA);
                                intent.setPackage("com.tencent.mm");

                                intent.putExtra(Intent.EXTRA_TEXT, text);
                                startActivity(Intent.createChooser(intent, "Share with"));

                            } catch (PackageManager.NameNotFoundException e) {
                                Toast.makeText(SuccessActivity.this, R.string.package_wechat_error, Toast.LENGTH_SHORT).show();
                            }

                            return true;
                        } else if (id == R.id.line) {
                            try {
                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.setType("text/plain");

                                String text = getIntent().getStringExtra("shareText");

                                pm.getPackageInfo("jp.naver.line.android", PackageManager.GET_META_DATA);
                                intent.setPackage("jp.naver.line.android");

                                intent.putExtra(Intent.EXTRA_TEXT, text);
                                startActivity(Intent.createChooser(intent, "Share with"));

                            } catch (PackageManager.NameNotFoundException e) {
                                Toast.makeText(SuccessActivity.this, R.string.package_line_error, Toast.LENGTH_SHORT).show();
                            }

                            return true;
                        }

                        return true;
                    }
                });

                popup.show();
            }
        });
    }

    private void setupActionBar() {

        // Inflate a "Done/Cancel" custom action bar view.
        final LayoutInflater inflater = (LayoutInflater) this.getSupportActionBar().getThemedContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View customActionBarView = inflater.inflate(R.layout.custom_action_bar, null);
        customActionBarView.findViewById(R.id.title);

        ImageButton closePostPageBtn = (ImageButton) customActionBarView.findViewById(R.id.closePostPage);

        closePostPageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                startActivity(intent);
            }
        });

        final ActionBar actionBar = this.getSupportActionBar();
        actionBar.setDisplayOptions(
                ActionBar.DISPLAY_SHOW_CUSTOM, ActionBar.DISPLAY_SHOW_CUSTOM |
                        ActionBar.DISPLAY_SHOW_HOME |
                        ActionBar.DISPLAY_SHOW_TITLE);

        actionBar.setCustomView(customActionBarView,
                new ActionBar.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                )
        );

        ImageButton shareSocialBtn = (ImageButton) findViewById(R.id.rightActionButton);
        shareSocialBtn.setImageResource(R.drawable.share);
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
