package com.salismail.cultishexchange;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.makeramen.roundedimageview.RoundedImageView;
import com.salismail.cultishexchange.clientapi.RestApi;
import com.salismail.cultishexchange.clientapi.responseData.models.PostSummary;
import com.salismail.cultishexchange.ui.activity.DialogsActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity
            implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener {

    //Signing Options
    private GoogleSignInOptions gso;

    //google api client
    private GoogleApiClient mGoogleApiClient;

    private SharedPreferences prefs;

    private GPSTracker tracker;

    private static int REQUEST_ACCESS_FINE_LOCATION_PERMISSION_RESULT = 1010;

    public List<PostSummary> postSummaryData = new ArrayList<PostSummary>();

    public RelativeLayout maskView;
    public TextView noDataView;
    int filterKey = 0;

    double latitude;
    double longitude;
    String userId;

    LinearLayout gridLayout1;
    LinearLayout gridLayout2;

    EditText searchText;

    Point size;

    InteractiveScrollView postScrollView;

    TextView nearbyText;
    RelativeLayout nearUnderline;

    TextView popularText;
    RelativeLayout popularUnderline;

    LinearLayout homePageTabs;
    LinearLayout customResultTab;

    TextView tabTextView;

    com.rey.material.widget.RelativeLayout goToInboxBtn;

    static int page = 0;
    static int searchIndex = 0;
    static String searchWord = "";

    public static HomeActivity instance;

    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        instance = this;

        // Set Device Size
        size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);

        // get google api client
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();



        FloatingActionButton newPostBtn = (FloatingActionButton) findViewById(R.id.newPost);
        newPostBtn.setOnClickListener(this);
        ImageView menuBtn = (ImageView) findViewById(R.id.menuBar);
        menuBtn.setOnClickListener(this);
        ImageView searchBtn = (ImageView) findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(this);
        ImageView prevBtn = (ImageView) findViewById(R.id.prevBar);
        prevBtn.setOnClickListener(this);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        goToInboxBtn = (com.rey.material.widget.RelativeLayout) findViewById(R.id.goToInbox);
        goToInboxBtn.setOnClickListener(this);

        nearbyText = (TextView) findViewById(R.id.nearText);
        nearUnderline = (RelativeLayout) findViewById(R.id.nearUnderline);

        popularText = (TextView) findViewById(R.id.popularText);
        popularUnderline = (RelativeLayout) findViewById(R.id.popularUnderline);

        homePageTabs = (LinearLayout) findViewById(R.id.homePageTabs);
        customResultTab = (LinearLayout) findViewById(R.id.customResultTab);

        tabTextView = (TextView) findViewById(R.id.tabText);

        postScrollView = (InteractiveScrollView) findViewById(R.id.homeScrollView);

        // Set Size to navigation background
        ImageView navBgImg = (ImageView) findViewById(R.id.navigationBg);
        navBgImg.getLayoutParams().height = size.y;
        navBgImg.getLayoutParams().width = size.y * (720 / 640);

        TextView userNameView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.userName);
        TextView userEmailView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.userEmail);
        ImageView noAvatar = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.noAvatar);
        ImageView avatar = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.avatar);

        prefs = getSharedPreferences("accountInfo", Context.MODE_PRIVATE);
        userId = prefs.getString("id", "");
        String userName = prefs.getString("name", "");
        String userEmail = prefs.getString("email", "");
        String avatarUrl = prefs.getString("avatarUrl", "");

        if (!avatarUrl.isEmpty()) {
            noAvatar.setVisibility(View.INVISIBLE);
            avatar.setVisibility(View.VISIBLE);
            Glide.with(getBaseContext()).load(avatarUrl).into(avatar);
        }

        userNameView.setText(userName);
        userEmailView.setText(userEmail);

        searchText = (EditText) findViewById(R.id.searchField);

        RelativeLayout nearbyTab = (RelativeLayout) findViewById(R.id.filterByNear);
        RelativeLayout popularTab = (RelativeLayout) findViewById(R.id.filterByPopular);
        nearbyTab.setOnClickListener(this);
        popularTab.setOnClickListener(this);

        maskView = (RelativeLayout) findViewById(R.id.loadingProgress);
        noDataView = (TextView) findViewById(R.id.noDataAlert);
        final float scale = getResources().getDisplayMetrics().density;

        RelativeLayout homeContainer = (RelativeLayout) findViewById(R.id.homeContainer);
        homeContainer.setMinimumHeight(size.y - (int) (135 * scale + 0.5f));

        gridLayout1 = (LinearLayout) findViewById(R.id.postGridLayout1);
        gridLayout2 = (LinearLayout) findViewById(R.id.postGridLayout2);

        // change status bar color
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(Color.BLACK);

        // request location permission
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                tracker = new GPSTracker(HomeActivity.this);

                if (tracker.isCanGetLocation()) {
                    latitude = tracker.getLatitude();
                    longitude = tracker.getLongitude();

                    getPostSummaryDataFromAPI();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);
                    alertDialog.setTitle(R.string.location_disabled);
                    alertDialog.setMessage(R.string.location_enable_confirm)
                            .setCancelable(false)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivity(myIntent);
                                }
                            });

                    AlertDialog dialog = alertDialog.create();
                    dialog.show();
                }

            } else {
                if(shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //@Todo: nothing
                }
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACCESS_FINE_LOCATION_PERMISSION_RESULT);
            }
        } else {
            tracker = new GPSTracker(HomeActivity.this);

            if (tracker.isCanGetLocation()) {
                latitude = tracker.getLatitude();
                longitude = tracker.getLongitude();

                getPostSummaryDataFromAPI();
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);
                alertDialog.setTitle(R.string.location_disabled);
                alertDialog.setMessage(R.string.location_enable_confirm)
                        .setCancelable(false)
                        .setPositiveButton(R.string.enable, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(myIntent);
                            }
                        });

                AlertDialog dialog = alertDialog.create();
                dialog.show();
            }
        }

        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // hide soft keyboard on android when it's showing up
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                    page = 0;
                    searchWord = searchText.getText().toString();
                    searchIndex = 1;
                    performSearch();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        RelativeLayout header = (RelativeLayout) findViewById(R.id.header);
        RelativeLayout searchBar = (RelativeLayout) findViewById(R.id.searchBar);

        if (v.getId() == R.id.newPost) {
            Intent intent = new Intent(getBaseContext(), CameraActivity.class);
            intent.putExtra("fromActivity", "home");
            SharedPreferences prefs = getSharedPreferences("postImages", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.remove("postId");
            editor.remove("brand");
            editor.remove("model");
            editor.remove("description");
            editor.remove("uriset");
            editor.remove("price");
            editor.remove("currency");
            editor.remove("imageNumber");
            editor.commit();
            startActivity(intent);

        } else if (v.getId() == R.id.menuBar) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                drawer.openDrawer(GravityCompat.START);
            }
        } else if (v.getId() == R.id.searchBtn) {
            header.setVisibility(View.INVISIBLE);
            searchBar.setVisibility(View.VISIBLE);
        } else if (v.getId() == R.id.prevBar) {
            header.setVisibility(View.VISIBLE);
            searchBar.setVisibility(View.INVISIBLE);
        } else if (v.getId() == R.id.filterByNear) {
            nearbyText.setTextColor(Color.parseColor("#ffffff"));
            nearUnderline.setBackgroundResource(R.drawable.tab_select);

            popularText.setTextColor(Color.parseColor("#aaaaaa"));
            popularUnderline.setBackgroundResource(R.drawable.tab_no_select);

            filterKey = 0;
            page = 0;
            getPostSummaryDataFromAPI();
        } else if (v.getId() == R.id.filterByPopular) {
            popularText.setTextColor(Color.parseColor("#ffffff"));
            popularUnderline.setBackgroundResource(R.drawable.tab_select);

            nearbyText.setTextColor(Color.parseColor("#aaaaaa"));
            nearUnderline.setBackgroundResource(R.drawable.tab_no_select);

            filterKey = 1;
            page = 0;
            getPostSummaryDataFromAPI();
        } else if (v.getId() == R.id.goToInbox) {
            DialogsActivity.start(HomeActivity.this);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
     public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            // Handle navigation view item clicks here.
            int id = item.getItemId();

            if (id == R.id.homePage) {
                page = 0;
                searchIndex = 0;
                homePageTabs.setVisibility(View.VISIBLE);
                customResultTab.setVisibility(View.INVISIBLE);

                nearbyText.setTextColor(Color.parseColor("#ffffff"));
                nearUnderline.setBackgroundResource(R.drawable.tab_select);

                popularText.setTextColor(Color.parseColor("#aaaaaa"));
                popularUnderline.setBackgroundResource(R.drawable.tab_no_select);

                filterKey = 0;
                getPostSummaryDataFromAPI();
            } else if (id == R.id.soldItem) {
                page = 0;
                searchIndex = 3;
                homePageTabs.setVisibility(View.INVISIBLE);
                customResultTab.setVisibility(View.VISIBLE);
                tabTextView.setText(R.string.sold_items);
                soldItemSearch();
            } else if (id == R.id.myItem) {
                page = 0;
                searchIndex = 4;
                homePageTabs.setVisibility(View.INVISIBLE);
                customResultTab.setVisibility(View.VISIBLE);
                tabTextView.setText(R.string.my_items);
                myItemSearch();
            } else if (id == R.id.inboxItem) {
                Intent intent = new Intent(getBaseContext(), InboxActivity.class);
                startActivity(intent);
            } else if (id == R.id.savedItem) {
                page = 0;
                searchIndex = 2;
                homePageTabs.setVisibility(View.INVISIBLE);
                customResultTab.setVisibility(View.VISIBLE);
                tabTextView.setText(R.string.saved);
                savedSearch();
            } else if (id == R.id.contactItem) {
                //@Todo: 3rd stage
            } else if (id == R.id.logoutItem) {
                signOut();
            }

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);

            return true;
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.remove("id");
                        editor.remove("name");
                        editor.remove("email");
                        editor.remove("givenName");
                        editor.remove("familyName");
                        editor.remove("avatarUrl");
                        editor.remove("saved");
                        editor.remove("liked");
                        editor.commit();

                        Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                        startActivity(intent);
                    }
                });
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this, R.string.connectionFailMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_ACCESS_FINE_LOCATION_PERMISSION_RESULT) {
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(),
                        R.string.access_denied, Toast.LENGTH_SHORT).show();
            } else {
                tracker = new GPSTracker(HomeActivity.this);

                if (tracker.isCanGetLocation()) {
                    latitude = tracker.getLatitude();
                    longitude = tracker.getLongitude();

                    page = 0;

                    getPostSummaryDataFromAPI();
                }
            }
        }
    }

    public void soldItemSearch() {
        maskView.setVisibility(View.VISIBLE);
        RestApi.Factory.getInstance().getSoldPost(latitude, longitude, userId, page).enqueue(new Callback<List<PostSummary>>() {
            @Override
            public void onResponse(Call<List<PostSummary>> call, Response<List<PostSummary>> response) {
                postSummaryData = response.body();
                if (postSummaryData.size() == 0) {
                    gridLayout1.removeAllViews();
                    gridLayout2.removeAllViews();
                    noDataView.setVisibility(View.VISIBLE);
                } else {
                    noDataView.setVisibility(View.INVISIBLE);
                    addNewSpannedView();
                }
                maskView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<List<PostSummary>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), R.string.connectionFailMsg, Toast.LENGTH_SHORT).show();

                gridLayout1.removeAllViews();
                gridLayout2.removeAllViews();

                maskView.setVisibility(View.INVISIBLE);
                noDataView.setVisibility(View.VISIBLE);
            }
        });
    }

    public void myItemSearch() {
        maskView.setVisibility(View.VISIBLE);
        RestApi.Factory.getInstance().getMyPost(latitude, longitude, userId, page).enqueue(new Callback<List<PostSummary>>() {
            @Override
            public void onResponse(Call<List<PostSummary>> call, Response<List<PostSummary>> response) {
                postSummaryData = response.body();
                if (postSummaryData.size() == 0) {
                    gridLayout1.removeAllViews();
                    gridLayout2.removeAllViews();
                    noDataView.setVisibility(View.VISIBLE);
                } else {
                    noDataView.setVisibility(View.INVISIBLE);
                    addNewSpannedView();
                }
                maskView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<List<PostSummary>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), R.string.connectionFailMsg, Toast.LENGTH_SHORT).show();

                gridLayout1.removeAllViews();
                gridLayout2.removeAllViews();

                maskView.setVisibility(View.INVISIBLE);
                noDataView.setVisibility(View.VISIBLE);
            }
        });
    }

    public void performSearch() {
        maskView.setVisibility(View.VISIBLE);
        RestApi.Factory.getInstance().getAllPost(latitude, longitude, 0, page, searchWord).enqueue(new Callback<List<PostSummary>>() {
            @Override
            public void onResponse(Call<List<PostSummary>> call, Response<List<PostSummary>> response) {
                postSummaryData = response.body();
                if (postSummaryData.size() == 0) {
                    gridLayout1.removeAllViews();
                    gridLayout2.removeAllViews();
                    noDataView.setVisibility(View.VISIBLE);
                } else {
                    noDataView.setVisibility(View.INVISIBLE);
                    addNewSpannedView();
                }
                maskView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<List<PostSummary>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), R.string.connectionFailMsg, Toast.LENGTH_SHORT).show();

                gridLayout1.removeAllViews();
                gridLayout2.removeAllViews();

                maskView.setVisibility(View.INVISIBLE);
                noDataView.setVisibility(View.VISIBLE);
            }
        });
    }

    public void getPostSummaryDataFromAPI() {
        maskView.setVisibility(View.VISIBLE);
        RestApi.Factory.getInstance().getAllPost(latitude, longitude, filterKey, page, "").enqueue(new Callback<List<PostSummary>>() {
            @Override
            public void onResponse(Call<List<PostSummary>> call, Response<List<PostSummary>> response) {
                postSummaryData = response.body();
                if (postSummaryData.size() == 0) {
                    gridLayout1.removeAllViews();
                    gridLayout2.removeAllViews();
                    noDataView.setVisibility(View.VISIBLE);
                } else {
                    noDataView.setVisibility(View.INVISIBLE);
                    addNewSpannedView();
                }
                maskView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<List<PostSummary>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), R.string.connectionFailMsg, Toast.LENGTH_SHORT).show();

                gridLayout1.removeAllViews();
                gridLayout2.removeAllViews();

                maskView.setVisibility(View.INVISIBLE);
                noDataView.setVisibility(View.VISIBLE);
            }
        });
    }

    public void savedSearch() {
        maskView.setVisibility(View.VISIBLE);
        RestApi.Factory.getInstance().getSavedPosts(latitude, longitude, userId, page).enqueue(new Callback<List<PostSummary>>() {
            @Override
            public void onResponse(Call<List<PostSummary>> call, Response<List<PostSummary>> response) {
                postSummaryData = response.body();
                if (postSummaryData.size() == 0) {
                    gridLayout1.removeAllViews();
                    gridLayout2.removeAllViews();
                    noDataView.setVisibility(View.VISIBLE);
                } else {
                    noDataView.setVisibility(View.INVISIBLE);
                    addNewSpannedView();
                }
                maskView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<List<PostSummary>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), R.string.connectionFailMsg, Toast.LENGTH_SHORT).show();

                gridLayout1.removeAllViews();
                gridLayout2.removeAllViews();

                maskView.setVisibility(View.INVISIBLE);
                noDataView.setVisibility(View.VISIBLE);
            }
        });
    }

    private void addNewSpannedView() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        gridLayout1.removeAllViews();
        gridLayout2.removeAllViews();

        float length1 = 0, length2 = 0;

        for (int i = 0; i < postSummaryData.size(); i++) {

            LinearLayout parentLayout;

            if (length1 > length2) {
                parentLayout = gridLayout2;
                length2 += postSummaryData.get(i).getRatio();
            } else {
                parentLayout = gridLayout1;
                length1 += postSummaryData.get(i).getRatio();
            }

            final View v = inflater.inflate(R.layout.post_summary_item, parentLayout, false);

            v.setTag(postSummaryData.get(i).getId());

            int padding_in_dp = 15;  // 15 dp
            final float scale = getResources().getDisplayMetrics().density;
            int padding_in_px = (int) (padding_in_dp * scale + 0.5f);

            v.getLayoutParams().width = (size.x - 3 * padding_in_px) / 2;
            v.getLayoutParams().height = Math.round((size.x - 3 * padding_in_px) / 2 * postSummaryData.get(i).getRatio());

            List<String> imageList = new ArrayList<String>(Arrays.asList(postSummaryData.get(i).getImages().split("\\s*,\\s*")));
            final String firstImage = imageList.get(0);

            final RoundedImageView postImage = (RoundedImageView) v.findViewById(R.id.postFirstImage);
            TextView distValue = (TextView) v.findViewById(R.id.distValue);
            TextView favorValue = (TextView) v.findViewById(R.id.viewValue);

            final double distance = postSummaryData.get(i).getDistance();
            String distStr = Math.round(distance) + " m";
            if (distance > 1000) {
                distStr = Math.round(distance / 1000) + " km";
            }
            int favors = postSummaryData.get(i).getFavorites();

            Glide.with(getBaseContext())
                    .load(firstImage)
                    .into(postImage);

            distValue.setText(distStr);
            favorValue.setText(favors + "");

            parentLayout.addView(v);
            

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getBaseContext(), PostDetailActivity.class);
                    intent.putExtra("postId", view.getTag().toString());
                    intent.putExtra("distance", distance);
                    intent.putExtra("myLat", latitude);
                    intent.putExtra("myLng", longitude);
                    startActivity(intent);
                }
            });
        }
    }
}
