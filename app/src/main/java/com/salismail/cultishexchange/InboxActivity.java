package com.salismail.cultishexchange;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rey.material.widget.ImageButton;

public class InboxActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout inboxHeader;
    ImageButton navToHomeBtn;
    ImageButton searchInboxBtn;

    RelativeLayout searchInboxBar;
    ImageButton prevInboxBar;
    EditText searchInboxField;

    com.rey.material.widget.RelativeLayout allSessions;
    TextView allSessionText;
    RelativeLayout allSessionUnderline;

    com.rey.material.widget.RelativeLayout buySessions;
    TextView buySessionText;
    RelativeLayout buySessionUnderline;

    com.rey.material.widget.RelativeLayout sellSessions;
    TextView sellSessionText;
    RelativeLayout sellSessionUnderline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);

        // change status bar color
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(Color.BLACK);

        inboxHeader = (RelativeLayout) findViewById(R.id.inboxHeader);
        navToHomeBtn = (ImageButton) findViewById(R.id.navigateToHome);
        navToHomeBtn.setOnClickListener(this);
        searchInboxBtn = (ImageButton) findViewById(R.id.searchInboxBtn);
        searchInboxBtn.setOnClickListener(this);

        searchInboxBar = (RelativeLayout) findViewById(R.id.searchInboxBar);
        prevInboxBar = (ImageButton) findViewById(R.id.prevInboxBar);
        prevInboxBar.setOnClickListener(this);
        searchInboxField = (EditText) findViewById(R.id.searchInboxField);

        allSessions = (com.rey.material.widget.RelativeLayout) findViewById(R.id.allSessions);
        allSessions.setOnClickListener(this);
        allSessionText = (TextView) findViewById(R.id.allSessionText);
        allSessionUnderline = (RelativeLayout) findViewById(R.id.allSessionUnderline);

        buySessions = (com.rey.material.widget.RelativeLayout) findViewById(R.id.buySessions);
        buySessions.setOnClickListener(this);
        buySessionText = (TextView) findViewById(R.id.buySessionsText);
        buySessionUnderline = (RelativeLayout) findViewById(R.id.buySessionsUnderline);

        sellSessions = (com.rey.material.widget.RelativeLayout) findViewById(R.id.sellSessions);
        sellSessions.setOnClickListener(this);
        sellSessionText = (TextView) findViewById(R.id.sellSessionsText);
        sellSessionUnderline = (RelativeLayout) findViewById(R.id.sellSessionsUnderline);
    }

    @Override
    public void onClick(View v) {

        Intent intent = new Intent(getBaseContext(), HomeActivity.class);

        switch (v.getId()) {
            case R.id.navigateToHome:
                startActivity(intent);
                break;
            case R.id.searchInboxBtn:
                inboxHeader.setVisibility(View.INVISIBLE);
                searchInboxBar.setVisibility(View.VISIBLE);
                break;
            case R.id.prevInboxBar:
                inboxHeader.setVisibility(View.VISIBLE);
                searchInboxBar.setVisibility(View.INVISIBLE);
                break;
            case R.id.allSessions:
                allSessionText.setTextColor(Color.parseColor("#ffffff"));
                allSessionUnderline.setBackgroundResource(R.drawable.tab_select);
                buySessionText.setTextColor(Color.parseColor("#aaaaaa"));
                buySessionUnderline.setBackgroundResource(R.drawable.tab_no_select);
                sellSessionText.setTextColor(Color.parseColor("#aaaaaa"));
                sellSessionUnderline.setBackgroundResource(R.drawable.tab_no_select);
                break;
            case R.id.buySessions:
                allSessionText.setTextColor(Color.parseColor("#aaaaaa"));
                allSessionUnderline.setBackgroundResource(R.drawable.tab_no_select);
                buySessionText.setTextColor(Color.parseColor("#ffffff"));
                buySessionUnderline.setBackgroundResource(R.drawable.tab_select);
                sellSessionText.setTextColor(Color.parseColor("#aaaaaa"));
                sellSessionUnderline.setBackgroundResource(R.drawable.tab_no_select);
                break;
            case R.id.sellSessions:
                allSessionText.setTextColor(Color.parseColor("#aaaaaa"));
                allSessionUnderline.setBackgroundResource(R.drawable.tab_no_select);
                buySessionText.setTextColor(Color.parseColor("#aaaaaa"));
                buySessionUnderline.setBackgroundResource(R.drawable.tab_no_select);
                sellSessionText.setTextColor(Color.parseColor("#ffffff"));
                sellSessionUnderline.setBackgroundResource(R.drawable.tab_select);
                break;
            default:
                break;
        }
    }
}
