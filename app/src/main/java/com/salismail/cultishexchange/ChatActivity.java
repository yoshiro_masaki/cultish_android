package com.salismail.cultishexchange;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rey.material.widget.ImageButton;

public class ChatActivity extends AppCompatActivity {

    ImageButton navigateToPostDetail;
    ImageView userChatAvatar;
    TextView userNameText;
    TextView userStatus;
    TextView brandModel;
    TextView priceValue;
    ImageView detailFirstImageChat;

    GridLayout messageGridLayout;
    EditText messageInput;
    ImageButton sendMessageBtn;

    LayoutInflater inflater;

    float scale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        navigateToPostDetail = (ImageButton) findViewById(R.id.navigateToPostDetail);
        userChatAvatar = (ImageView) findViewById(R.id.userChatAvatar);
        userNameText = (TextView) findViewById(R.id.userNameText);
        userStatus = (TextView) findViewById(R.id.userStatus);
        brandModel = (TextView) findViewById(R.id.brandModel);
        priceValue = (TextView) findViewById(R.id.priceValue);
        detailFirstImageChat = (ImageView) findViewById(R.id.detailFirstImageChat);

        messageGridLayout = (GridLayout) findViewById(R.id.messageGridLayout);
        messageInput = (EditText) findViewById(R.id.messageInput);
        sendMessageBtn = (ImageButton) findViewById(R.id.sendMessageBtn);
        scale = getResources().getDisplayMetrics().density;

        if (getIntent().hasExtra("userEmail")) {
            String userFirstName = getIntent().getStringExtra("userFirstName");
            userNameText.setText(userFirstName);

            String userAvatarUrl = getIntent().getStringExtra("userAvatar");
            if (userAvatarUrl != "" && userAvatarUrl != null) {
                Glide.with(getBaseContext()).load(userAvatarUrl).into(userChatAvatar);
            }

            boolean isBuyer = getIntent().getBooleanExtra("isBuyer", true);
            if (!isBuyer) {
                userStatus.setText(getResources().getString(R.string.seller));
            }

            String brandModelTitle = getIntent().getStringExtra("brandModel");
            brandModel.setText(brandModelTitle);
            if (brandModelTitle.equals("")) {
                brandModel.setVisibility(View.GONE);
            }

            String currency = getIntent().getStringExtra("currency");
            int price = getIntent().getIntExtra("price", 0);
            if (price == 0) {
                priceValue.setText(getResources().getString(R.string.negotiable));
            } else {
                priceValue.setText(currency + " " + price);
            }

            String firstImageUrl = getIntent().getStringExtra("postImgUrl");
            Glide.with(getBaseContext()).load(firstImageUrl).override((int) (30 * scale + 0.5f), (int) (30 * scale + 0.5f)).centerCrop().into(detailFirstImageChat);
        }

        sendMessageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!messageInput.getText().toString().equals("")) {
                    View rowView = inflater.inflate(R.layout.chat_message_row, messageGridLayout, false);
                    ((LinearLayout)rowView.findViewById(R.id.sentMessage)).setVisibility(View.VISIBLE);
                    ((TextView)rowView.findViewById(R.id.sentMessageContent)).setText(messageInput.getText().toString());
                    messageGridLayout.addView(rowView);
                }
            }
        });
    }
}
