package com.salismail.cultishexchange;

import java.util.Date;

/**
 * Created by swordmaster on 7/16/2016.
 */
public class Message {
    public String msgId;
    public String msgContent;
    public Date msgTimestamp;
    public boolean isAuthorBuyer;
    public boolean isReadMsg;

    public Message(String msg_id, String msg_content, Date msg_timer, boolean is_buyer, boolean is_readMsg) {
        this.msgId = msg_id;
        this.msgContent = msg_content;
        this.msgTimestamp = msg_timer;
        this.isAuthorBuyer = is_buyer;
        this.isReadMsg = is_readMsg;
    }
}
