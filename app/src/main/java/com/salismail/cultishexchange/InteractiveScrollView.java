package com.salismail.cultishexchange;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.salismail.cultishexchange.clientapi.RestApi;
import com.salismail.cultishexchange.clientapi.responseData.models.PostSummary;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by swordmaster on 6/21/2016.
 */
public class InteractiveScrollView extends ScrollView {
    OnBottomReachedListener mListener;

    public InteractiveScrollView(Context context, AttributeSet attrs,
                                 int defStyle) {
        super(context, attrs, defStyle);
    }

    public InteractiveScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InteractiveScrollView(Context context) {
        super(context);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        View view = (View) getChildAt(getChildCount()-1);
        int diff = (view.getBottom()-(getHeight()+getScrollY()));

        if (diff == 0) {

            if (HomeActivity.instance.postSummaryData.size() < 20)
                return;

            HomeActivity.instance.page++;

            if (HomeActivity.instance.searchIndex == 0) {
                HomeActivity.instance.getPostSummaryDataFromAPI();
            } else if (HomeActivity.instance.searchIndex == 1) {
                HomeActivity.instance.performSearch();
            } else if (HomeActivity.instance.searchIndex == 2) {
                HomeActivity.instance.savedSearch();
            } else if (HomeActivity.instance.searchIndex == 3) {
                HomeActivity.instance.soldItemSearch();
            } else if (HomeActivity.instance.searchIndex == 4) {
                HomeActivity.instance.myItemSearch();
            }
        }

        if (getScrollY() == 0) {

            if (HomeActivity.instance.page == 0)
                return;

            HomeActivity.instance.page--;

            if (HomeActivity.instance.searchIndex == 0) {
                HomeActivity.instance.getPostSummaryDataFromAPI();
            } else if (HomeActivity.instance.searchIndex == 1) {
                HomeActivity.instance.performSearch();
            } else if (HomeActivity.instance.searchIndex == 2) {
                HomeActivity.instance.savedSearch();
            } else if (HomeActivity.instance.searchIndex == 3) {
                HomeActivity.instance.soldItemSearch();
            } else if (HomeActivity.instance.searchIndex == 4) {
                HomeActivity.instance.myItemSearch();
            }
        }

        super.onScrollChanged(l, t, oldl, oldt);
    }


    // Getters & Setters

    public OnBottomReachedListener getOnBottomReachedListener() {
        return mListener;
    }

    public void setOnBottomReachedListener(
            OnBottomReachedListener onBottomReachedListener) {
        mListener = onBottomReachedListener;
    }


    /**
     * Event listener.
     */
    public interface OnBottomReachedListener{
        public void onBottomReached();
    }
}
