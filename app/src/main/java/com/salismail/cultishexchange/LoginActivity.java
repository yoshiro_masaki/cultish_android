package com.salismail.cultishexchange;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.sample.core.ui.dialog.ProgressDialogFragment;
import com.quickblox.sample.core.utils.ErrorUtils;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.salismail.cultishexchange.clientapi.RestApi;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import com.salismail.cultishexchange.clientapi.responseData.models.FbUser;
import com.salismail.cultishexchange.clientapi.responseData.models.SigininResponse;
import com.salismail.cultishexchange.ui.activity.DialogsActivity;
import com.salismail.cultishexchange.utils.Consts;
import com.salismail.cultishexchange.utils.SharedPreferencesUtil;
import com.salismail.cultishexchange.utils.chat.ChatHelper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends FragmentActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    //Signing Options
    private GoogleSignInOptions gso;

    //google api client
    private GoogleApiClient mGoogleApiClient;

    //Signin constant to check the activity result
    private int RC_SIGN_IN = 100;

    SharedPreferences qbUserPrefs;
    SharedPreferences.Editor qbUserEditor;

    private CallbackManager callbackManager;
    private LoginButton loginButton;
    FbUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login_1);

        // change status bar color
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(Color.BLACK);

        //Initializing google signin option
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.server_client_id))
                .build();

        qbUserPrefs = getSharedPreferences("qb_user_info", Context.MODE_PRIVATE);
        qbUserEditor = qbUserPrefs.edit();

        //Initializing signinbutton
        RelativeLayout googleBtn = (RelativeLayout) findViewById(R.id.loginGoogle);

        //Initializing google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //Setting onclick listener to signing button
        googleBtn.setOnClickListener(this);

        RelativeLayout btnLogin = (RelativeLayout) findViewById(R.id.loginFb);
        btnLogin.setOnClickListener(this);

        callbackManager=CallbackManager.Factory.create();
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("public_profile", "email");
    }

    private void googleSignIn() {
        //Creating an intent
        Intent googleSignInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);

        //Starting intent for result
        startActivityForResult(googleSignInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        //If signin
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    @Override
    public void onBackPressed()
    {
        return;
    }

    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed

        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();
            String token = acct.getIdToken();

            //post token to server
            RestApi.Factory.getInstance().getInfoByToken(token).enqueue(new Callback<SigininResponse>() {
                @Override
                public void onResponse(Call<SigininResponse> call, final Response<SigininResponse> response) {
                    boolean isSuccess = response.body().getIsSuccess();
                    if (isSuccess) {
                        SharedPreferences prefs = getSharedPreferences("accountInfo", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("id", response.body().getUser().getId());
                        editor.putString("name", response.body().getUser().getName());
                        editor.putString("email", response.body().getUser().getEmail());
                        editor.putString("givenName", response.body().getUser().getGivenName());
                        editor.putString("familyName", response.body().getUser().getFamilyName());
                        editor.putString("avatarUrl", response.body().getUser().getPictureUrl());
                        editor.putStringSet("saved", new HashSet<String>(Arrays.asList(response.body().getUser().getSaved().split("\\s*,\\s*"))));
                        editor.putStringSet("liked", new HashSet<String>(Arrays.asList(response.body().getUser().getLiked().split("\\s*,\\s*"))));
                        editor.commit();

                        QBUsers.getUserByEmail(response.body().getUser().getEmail(), new QBEntityCallback<QBUser>() {
                            @Override
                            public void onSuccess(QBUser qbUser, Bundle bundle) {
                                QBUser user = new QBUser(qbUser.getLogin(), Consts.QB_USERS_PASSWORD);

                                QBUsers.signIn(user, new QBEntityCallback<QBUser>() {
                                    @Override
                                    public void onSuccess(final QBUser qbUser, Bundle bundle) {
                                        qbUser.setPassword(Consts.QB_USERS_PASSWORD);
                                        ChatHelper.getInstance().login(qbUser, new QBEntityCallback<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid, Bundle bundle) {
                                                SharedPreferencesUtil.saveQbUser(qbUser);

                                                finish();

                                                Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                                                startActivity(intent);
                                            }

                                            @Override
                                            public void onError(QBResponseException e) {
                                                Toast.makeText(getBaseContext(), "can not log in chat service!", Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }

                                    @Override
                                    public void onError(QBResponseException e) {
                                        Toast.makeText(getBaseContext(), "log in failure", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }

                            @Override
                            public void onError(QBResponseException e) {
                                QBUser user = new QBUser();
                                user.setLogin(response.body().getUser().getId());
                                user.setPassword(Consts.QB_USERS_PASSWORD);
                                user.setEmail(response.body().getUser().getEmail());
                                user.setFullName(response.body().getUser().getName());

                                QBUsers.signUp(user, new QBEntityCallback<QBUser>() {
                                    @Override
                                    public void onSuccess(final QBUser qbUser, Bundle bundle) {
                                        qbUser.setPassword(Consts.QB_USERS_PASSWORD);
                                        ChatHelper.getInstance().login(qbUser, new QBEntityCallback<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid, Bundle bundle) {
                                                SharedPreferencesUtil.saveQbUser(qbUser);

                                                Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                                                startActivity(intent);
                                            }

                                            @Override
                                            public void onError(QBResponseException e) {
                                                Toast.makeText(getBaseContext(), "can not log in chat service!", Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }

                                    @Override
                                    public void onError(QBResponseException e) {
                                        Toast.makeText(getBaseContext(), "Can not sign up", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });

                        /*ChatHelper.getInstance().login(user, new QBEntityCallback<Void>() {
                            @Override
                            public void onSuccess(Void result, Bundle bundle) {
                                Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                                startActivity(intent);
                            }

                            @Override
                            public void onError(QBResponseException e) {
                                Toast.makeText(getBaseContext(), "Can not log in to chat service", Toast.LENGTH_LONG).show();
                            }
                        });*/
                    } else {
                        Toast.makeText(LoginActivity.this, R.string.alreadyhasFacebookAccount, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SigininResponse> call, Throwable t) {
                    Toast.makeText(LoginActivity.this, R.string.connectionFailMsg, Toast.LENGTH_SHORT).show();
                }
            });

            //SharedPreference instance definition
            /*SharedPreferences prefs = getSharedPreferences("googleAccountInfo", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("userName", acct.getDisplayName());
            editor.putString("userEmail", acct.getEmail());
            Uri personPhoto = acct.getPhotoUrl();
            if (personPhoto != null) {
                editor.putString("avatarUrl", personPhoto.toString());
            }
            editor.commit();

            Intent intent = new Intent(getBaseContext(), HomeActivity.class);
            startActivity(intent);*/

        } else {
            //If login fails
            Toast.makeText(this, R.string.loginFailMsg, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.loginGoogle) {
            //Calling signin
            googleSignIn();
        } else if (v.getId() == R.id.loginFb) {
            facebookLogIn();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this, R.string.connectionFailMsg, Toast.LENGTH_LONG).show();
    }

    public void facebookLogIn() {
        loginButton.performClick();

        loginButton.setPressed(true);

        loginButton.invalidate();

        loginButton.registerCallback(callbackManager, mCallBack);

        loginButton.setPressed(false);

        loginButton.invalidate();
    }


    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            // App code
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {

                            try {
                                user = new FbUser();
                                user.facebookID = object.getString("id").toString();
                                user.email = object.getString("email").toString();
                                user.name = object.getString("name").toString();
                                user.gender = object.getString("gender").toString();

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }

                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender, birthday");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };
}
