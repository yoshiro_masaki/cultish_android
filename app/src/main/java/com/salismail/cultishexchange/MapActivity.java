package com.salismail.cultishexchange;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends AppCompatActivity {

    double myLat, myLng, sellerLat, sellerLng;
    MapFragment mapView;
    GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        myLat = getIntent().getDoubleExtra("myLat", 0);
        myLng = getIntent().getDoubleExtra("myLng", 0);
        sellerLat = getIntent().getDoubleExtra("sellerLat", 0);
        sellerLng = getIntent().getDoubleExtra("sellerLng", 0);

        mapView = (MapFragment) getFragmentManager().findFragmentById(R.id.fullScreenMap);
        googleMap = mapView.getMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);

        LatLng myPosition = new LatLng(myLat, myLng);
        LatLng sellerPosition = new LatLng(sellerLat, sellerLng);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sellerPosition, 17));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);

        CircleOptions circleOptions = new CircleOptions()
                .center(sellerPosition)
                .radius(250)
                .fillColor(Color.parseColor("#8c32cbff"))
                .strokeColor(Color.parseColor("#8c32cbff"))
                .strokeWidth(0);

        googleMap.addCircle(circleOptions);
    }
}
