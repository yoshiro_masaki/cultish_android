
package com.salismail.cultishexchange.clientapi.responseData.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class UserInfo {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("givenName")
    @Expose
    private String givenName;
    @SerializedName("familyName")
    @Expose
    private String familyName;
    @SerializedName("pictureUrl")
    @Expose
    private String pictureUrl;
    @SerializedName("isGoogle")
    @Expose
    private Boolean isGoogle;
    @SerializedName("createAt")
    @Expose
    private String createAt;
    @SerializedName("updateAt")
    @Expose
    private String updateAt;
    @SerializedName("liked")
    @Expose
    private String liked;
    @SerializedName("saved")
    @Expose
    private String saved;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The _id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The givenName
     */
    public String getGivenName() {
        return givenName;
    }

    /**
     * 
     * @param givenName
     *     The givenName
     */
    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    /**
     * 
     * @return
     *     The familyName
     */
    public String getFamilyName() {
        return familyName;
    }

    /**
     * 
     * @param familyName
     *     The familyName
     */
    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    /**
     * 
     * @return
     *     The pictureUrl
     */
    public String getPictureUrl() {
        return pictureUrl;
    }

    /**
     * 
     * @param pictureUrl
     *     The pictureUrl
     */
    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    /**
     * 
     * @return
     *     The isGoogle
     */
    public Boolean getIsGoogle() {
        return isGoogle;
    }

    /**
     * 
     * @param isGoogle
     *     The isGoogle
     */
    public void setIsGoogle(Boolean isGoogle) {
        this.isGoogle = isGoogle;
    }

    /**
     * 
     * @return
     *     The createAt
     */
    public String getCreateAt() {
        return createAt;
    }

    /**
     * 
     * @param createAt
     *     The createAt
     */
    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    /**
     * 
     * @return
     *     The updateAt
     */
    public String getUpdateAt() {
        return updateAt;
    }

    /**
     * 
     * @param updateAt
     *     The updateAt
     */
    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    /**
     *
     * @return
     *     The saved
     */
    public String getSaved() {
        return saved;
    }

    /**
     *
     * @param saved
     *     The saved
     */
    public void setSaved(String saved) {
        this.saved = saved;
    }

    /**
     *
     * @return
     *     The liked
     */
    public String getLiked() {
        return liked;
    }

    /**
     *
     * @param liked
     *     The liked
     */
    public void setLiked(String liked) {
        this.liked = liked;
    }

}
