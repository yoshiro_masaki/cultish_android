
package com.salismail.cultishexchange.clientapi.responseData.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class PostSummary {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("images")
    @Expose
    private String images;
    @SerializedName("favorites")
    @Expose
    private Integer favorites;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("ratio")
    @Expose
    private Float ratio;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The _id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The images
     */
    public String getImages() {
        return images;
    }

    /**
     * 
     * @param images
     *     The images
     */
    public void setImages(String images) {
        this.images = images;
    }

    /**
     * 
     * @return
     *     The favorites
     */
    public Integer getFavorites() {
        return favorites;
    }

    /**
     * 
     * @param views
     *     The views
     */
    public void setFavorites(Integer favorites) {
        this.favorites = favorites;
    }

    /**
     * 
     * @return
     *     The distance
     */
    public Double getDistance() {
        return distance;
    }

    /**
     * 
     * @param distance
     *     The distance
     */
    public void setDistance(Double distance) {
        this.distance = distance;
    }

    /**
     *
     * @return
     *     The ratio
     */
    public Float getRatio() {
        return ratio;
    }

    /**
     *
     * @param ratio
     *     The ratio
     */
    public void setRatio(Float ratio) {
        this.ratio = ratio;
    }

}
