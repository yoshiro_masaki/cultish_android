package com.salismail.cultishexchange.clientapi.requestData;

/**
 * Created by swordmaster on 5/25/2016.
 */
public class Post {
    private String _id;
    private String _userId;
    private String _brand;
    private String _model;
    private String _description;
    private float _price;
    private String _currency;
    private double _latitude;
    private double _longitude;
    private float _ratio;
    private String _imageUris;

    public Post(String id, String userId, String brand, String model, String description, float price, String currency, double latitude, double longitude, float ratio, String imageUris) {
        _id = id;
        _userId = userId;
        _brand = brand;
        _model = model;
        _description = description;
        _price = price;
        _currency = currency;
        _latitude = latitude;
        _longitude = longitude;
        _ratio = ratio;
        _imageUris = imageUris;
    }
}