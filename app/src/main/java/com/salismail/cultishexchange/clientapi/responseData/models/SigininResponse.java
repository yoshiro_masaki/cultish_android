
package com.salismail.cultishexchange.clientapi.responseData.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class SigininResponse {

    @SerializedName("isSuccess")
    @Expose
    private Boolean isSuccess;
    @SerializedName("user")
    @Expose
    private User user;

    /**
     * 
     * @return
     *     The isSuccess
     */
    public Boolean getIsSuccess() {
        return isSuccess;
    }

    /**
     * 
     * @param isSuccess
     *     The isSuccess
     */
    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    /**
     * 
     * @return
     *     The user
     */
    public User getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    public void setUser(User user) {
        this.user = user;
    }

}
