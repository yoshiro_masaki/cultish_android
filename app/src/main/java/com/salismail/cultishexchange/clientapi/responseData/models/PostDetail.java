
package com.salismail.cultishexchange.clientapi.responseData.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class PostDetail {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("favorites")
    @Expose
    private Integer favorites;
    @SerializedName("images")
    @Expose
    private String images;
    @SerializedName("createAt")
    @Expose
    private String createAt;
    @SerializedName("updateAt")
    @Expose
    private String updateAt;
    @SerializedName("userInfo")
    @Expose
    private UserInfo userInfo;
    @SerializedName("ratio")
    @Expose
    private Float ratio;
    @SerializedName("sold")
    @Expose
    private Boolean sold;
    @SerializedName("flag")
    @Expose
    private Boolean flag;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The _id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 
     * @param userId
     *     The userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 
     * @return
     *     The brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * 
     * @param brand
     *     The brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * 
     * @return
     *     The model
     */
    public String getModel() {
        return model;
    }

    /**
     * 
     * @param model
     *     The model
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The price
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    /**
     * 
     * @return
     *     The currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * 
     * @param currency
     *     The currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * 
     * @return
     *     The location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * 
     * @param location
     *     The location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * 
     * @return
     *     The favorites
     */
    public Integer getFavorites() {
        return favorites;
    }

    /**
     * 
     * @param views
     *     The views
     */
    public void setFavorites(Integer favorites) {
        this.favorites = favorites;
    }

    /**
     * 
     * @return
     *     The images
     */
    public String getImages() {
        return images;
    }

    /**
     * 
     * @param images
     *     The images
     */
    public void setImages(String images) {
        this.images = images;
    }

    /**
     * 
     * @return
     *     The createAt
     */
    public String getCreateAt() {
        return createAt;
    }

    /**
     * 
     * @param createAt
     *     The createAt
     */
    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    /**
     * 
     * @return
     *     The updateAt
     */
    public String getUpdateAt() {
        return updateAt;
    }

    /**
     * 
     * @param updateAt
     *     The updateAt
     */
    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    /**
     * 
     * @return
     *     The userInfo
     */
    public UserInfo getUserInfo() {
        return userInfo;
    }

    /**
     * 
     * @param userInfo
     *     The userInfo
     */
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    /**
     *
     * @return
     *     The ratio
     */
    public Float getRatio() {
        return ratio;
    }

    /**
     *
     * @param ratio
     *     The ratio
     */
    public void setRatio(Float ratio) {
        this.ratio = ratio;
    }

    /**
     *
     * @return
     *     The sold
     */
    public Boolean getSold() {
        return sold;
    }

    /**
     *
     * @param sold
     *     The sold
     */
    public void setSold(Boolean sold) {
        this.sold = sold;
    }

    /**
     *
     * @return
     *     The flag
     */
    public Boolean getFlag() {
        return flag;
    }

    /**
     *
     * @param flag
     *     The flag
     */
    public void setFlag(Boolean flag) {
        this.flag = flag;
    }
}
