package com.salismail.cultishexchange.clientapi;

import com.salismail.cultishexchange.clientapi.requestData.Post;
import com.salismail.cultishexchange.clientapi.responseData.models.PostDetail;
import com.salismail.cultishexchange.clientapi.responseData.models.PostSummary;
import com.salismail.cultishexchange.clientapi.responseData.models.SigininResponse;

import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by swordmaster on 5/9/2016.
 * This is rest API for retrieving data from server
 */
public interface RestApi {
    String BASE_URL = "http://cultishapp.herokuapp.com/";
    //String BASE_URL = "https://cultishapi-swordmaster125.c9users.io/";

    // API to post google token to server
    @GET("googlesignin")
    Call<SigininResponse> getInfoByToken(@Query("token") String token);

    // API to create new post
    @Multipart
    @POST("createPost")
    Call<Boolean> createPost(@Part("post") Post post, @PartMap Map<String, RequestBody> mapFiles);

    // API to update new post
    @Multipart
    @PUT("updatePost")
    Call<Boolean> updatePost(@Part("post") Post post, @PartMap Map<String, RequestBody> mapFiles);

    /** API to get all posts by nearby or popular
     * filterKey = 0 : nearby
     * filterKey = 1 : popular
     */
    @GET("posts")
    Call<List<PostSummary>> getAllPost(@Query("latitude") double latitude, @Query("longitude") double longitude, @Query("filterKey") int key, @Query("page") int page, @Query("keyStr") String str);

    @GET("posts/{id}")
    Call<PostDetail> getPostById(@Path("id") String postId);

    @GET("users/saved")
    Call<Boolean> updateSavedData(@Query("id") String userId, @Query("saved") String saved);

    @GET("users/liked")
    Call<Boolean> updateLikedData(@Query("id") String userId, @Query("liked") String liked, @Query("postId") String postId);

    @GET("posts/saved")
    Call<List<PostSummary>> getSavedPosts(@Query("latitude") double latitude, @Query("longitude") double longitude, @Query("userId") String userId, @Query("page") int page);

    @GET("posts/sold")
    Call<List<PostSummary>> getSoldPost(@Query("latitude") double latitude, @Query("longitude") double longitude, @Query("userId") String userId, @Query("page") int page);

    @GET("posts/available")
    Call<List<PostSummary>> getMyPost(@Query("latitude") double latitude, @Query("longitude") double longitude, @Query("userId") String userId, @Query("page") int page);

    @DELETE("posts/{id}")
    Call<Boolean> deletePost(@Path("id") String postId);

    @GET("posts/{id}/setFlag")
    Call<Boolean> setFlag(@Path("id") String postId);

    @GET("posts/{id}/setSold")
    Call<Boolean> setSold(@Path("id") String postId);

    // API instance class
    class Factory {
        private static RestApi service;

        public static RestApi getInstance() {
            if (service == null) {
                Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(BASE_URL).build();
                service = retrofit.create(RestApi.class);
                return service;
            } else {
                return service;
            }
        }
    }
}