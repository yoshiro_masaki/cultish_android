package com.salismail.cultishexchange;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.os.Bundle;
import android.os.HandlerThread;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;

public class SplashActivity extends Activity {

    Handler handler;
    TextView textView;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_1);

        // change status bar color
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(Color.BLACK);

        handler = new Handler();
        textView = (TextView) findViewById(R.id.loadingDot);

        SharedPreferences prefs = getSharedPreferences("accountInfo", Context.MODE_PRIVATE);
        userId = prefs.getString("id", "");

        setLoading();

        if (userId != null && !userId.isEmpty() && !userId.equals(null)) {
            finish();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                    Intent i = new Intent(getBaseContext(), HomeActivity.class);
                    startActivity(i);
                }
            }, 3000);
        } else {
            QBAuth.createSession(new QBEntityCallback<QBSession>() {
                @Override
                public void onSuccess(QBSession result, Bundle params) {
                    finish();
                    Intent i = new Intent(getBaseContext(), LoginActivity.class);
                    startActivity(i);
                }

                @Override
                public void onError(QBResponseException e) {
                    Toast.makeText(getBaseContext(), "Can't create chat session", Toast.LENGTH_LONG).show();
                }
            });
        }

        /*for (int i = 400; i <= 3600; i+=400) {
            final int j = i;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (j % 1200 == 0) {
                        textView.setText("...");
                    } else if (j % 1200 == 800) {
                        textView.setText("..");
                    } else if (j % 1200 == 400) {
                        textView.setText(".");
                    }

                    if (j == 3600) {

                        SharedPreferences prefs = getSharedPreferences("accountInfo", Context.MODE_PRIVATE);
                        String userId = prefs.getString("id", "");

                        if (userId != null && !userId.isEmpty() && !userId.equals(null)) {
                            Intent i = new Intent(getBaseContext(), HomeActivity.class);
                            //Intent i = new Intent(getBaseContext(), PostActivity.class);
                            startActivity(i);
                        } else {
                            Intent i = new Intent(getBaseContext(), LoginActivity.class);
                            startActivity(i);
                        }
                    }
                }
            }, j);
        }*/
    }

    public void setLoading() {
        for (int i = 400; i <= 10000; i += 400) {
            final int j = i;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (j % 1200 == 0) {
                        textView.setText("...");
                    } else if (j % 1200 == 800) {
                        textView.setText("..");
                    } else if (j % 1200 == 400) {
                        textView.setText(".");
                    }
                }
            }, j);
        }
    }
}