package com.salismail.cultishexchange;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.salismail.cultishexchange.clientapi.RestApi;
import com.salismail.cultishexchange.clientapi.requestData.Post;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostActivity extends AppCompatActivity {

    private static final int NUM_ROWS = 2;
    private static final int NUM_COLS = 3;

    private EditText brandField;
    private EditText modelField;
    private EditText descriptionField;
    private EditText priceField;
    private TextView countLayout;
    private Button postButton;
    private MaterialBetterSpinner currencySpinner;

    private GPSTracker tracker;
    private double latitude;
    private double longitude;

    private static int REQUEST_ACCESS_FINE_LOCATION_PERMISSION_RESULT = 1010;

    float scale;
    GridLayout picturesLayout;
    RelativeLayout postCoverLayout;
    Point size;
    int padding;
    List<String> uriList;
    String[] CURRENCIES = {"USD", "GBP", "HKD", "EUR"};
    SharedPreferences prefs;

    File mCompressionImageFolder;

    // Show the current focused editText
    EditText currentFocusedView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        prefs = getSharedPreferences("postImages", Context.MODE_PRIVATE);

        // change status bar color
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(Color.BLACK);

        setupActionBar();
        populatePhotoLayouts();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, CURRENCIES);
        MaterialBetterSpinner materialDesignSpinner = (MaterialBetterSpinner)
                findViewById(R.id.currencyField);
        materialDesignSpinner.setAdapter(arrayAdapter);
        materialDesignSpinner.setText(CURRENCIES[0]);

        descriptionField = (EditText) findViewById(R.id.descriptionField);
        countLayout = (TextView) findViewById(R.id.countLength);
        brandField = (EditText) findViewById(R.id.brandField);
        modelField = (EditText) findViewById(R.id.modelField);
        priceField = (EditText) findViewById(R.id.priceField);
        postButton = (Button) findViewById(R.id.postBtn);
        currencySpinner = (MaterialBetterSpinner) findViewById(R.id.currencyField);

        if (!prefs.getString("brand", "").equals("")) {
            brandField.setText(prefs.getString("brand", ""));
        }

        if (!prefs.getString("model", "").equals("")) {
            modelField.setText(prefs.getString("model", ""));
        }

        if (!prefs.getString("description", "").equals("")) {
            descriptionField.setText(prefs.getString("description", ""));
        }

        if (prefs.getInt("price", 0) != 0) {
            priceField.setText(prefs.getInt("price", 0) + "");
        }

        if (!prefs.getString("currency", "").equals("")) {
            currencySpinner.setText(prefs.getString("currency", ""));
        }

        if (!prefs.getString("description", "").equals("")) {
            countLayout.setText(prefs.getString("description", "").length() + "/800");
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                tracker = new GPSTracker(PostActivity.this);

                if (tracker.isCanGetLocation()) {
                    latitude = tracker.getLatitude();
                    longitude = tracker.getLongitude();
                }

            } else {
                if(shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //@Todo: nothing
                }
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACCESS_FINE_LOCATION_PERMISSION_RESULT);
            }
        } else {
            tracker = new GPSTracker(PostActivity.this);

            if (tracker.isCanGetLocation()) {
                latitude = tracker.getLatitude();
                longitude = tracker.getLongitude();
            }
        }

        brandField.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (brandField.getText().length() == 0) {
                    brandField.setBackgroundResource(R.drawable.add_text_layout_background);
                } else {
                    brandField.setBackgroundResource(R.drawable.add_text_layout_background_2);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        modelField.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (modelField.getText().length() == 0) {
                    modelField.setBackgroundResource(R.drawable.add_text_layout_background);
                } else {
                    modelField.setBackgroundResource(R.drawable.add_text_layout_background_2);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        priceField.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (priceField.getText().length() == 0) {
                    priceField.setBackgroundResource(R.drawable.add_text_layout_background);
                } else {
                    priceField.setBackgroundResource(R.drawable.add_text_layout_background_2);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        descriptionField.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //here is your code
                String lengthText = descriptionField.getText().length() + "/800";
                countLayout.setText(lengthText);
                if (descriptionField.getText().length() == 0) {
                    descriptionField.setBackgroundResource(R.drawable.add_text_layout_background);
                    countLayout.setTextColor(Color.parseColor("#e5e5e5"));
                } else {
                    descriptionField.setBackgroundResource(R.drawable.add_text_layout_background_2);
                    countLayout.setTextColor(Color.parseColor("#7f757f"));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // hide soft keyboard on android when it's showing up
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm.isAcceptingText()) {
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                }

                final MediaType MEDIA_TYPE = MediaType.parse("image/*");
                String uriSet = prefs.getString("uriset", "");

                if (uriSet.equals("")) {
                    Toast.makeText(getBaseContext(), R.string.no_image, Toast.LENGTH_SHORT).show();
                    return;
                } else if (!isValidPrice(priceField.getText().toString())) {
                    Toast.makeText(getBaseContext(), R.string.price_invalid, Toast.LENGTH_SHORT).show();
                    return;
                }

                final RelativeLayout maskLayout = (RelativeLayout) findViewById(R.id.maskLayout);
                maskLayout.setVisibility(View.VISIBLE);

                SharedPreferences accountPrefs = getSharedPreferences("accountInfo", Context.MODE_PRIVATE);
                String userId = accountPrefs.getString("id", "");

                String postId = prefs.getString("postId", "");

                if (postId.equals("")) {
                    // submit new post

                    List<String> uriListArray = new ArrayList<String>(Arrays.asList(uriSet.split("\\s*,\\s*")));
                    List<String> uriList = new ArrayList<String>(0);
                    for (String uri : uriListArray) {
                        if (!uri.equals("")) {
                            uriList.add(uri);
                        }
                    }
                    HashMap<String, RequestBody> map = new HashMap<>(uriList.size());
                    RequestBody file = null;
                    File f = null;
                    for (int i = 0; i < uriList.size(); i++) {
                        String imageAbsolutePath = new File(uriList.get(i)).getAbsolutePath();
                        Bitmap bmp = decodeSampledBitmapFromUri(uriList.get(i));
                        Bitmap realBmp = null;
                        try {
                            realBmp = modifyOrientation(bmp, imageAbsolutePath);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        f = getCompressedFile(realBmp, i);
                        file = RequestBody.create(MEDIA_TYPE, f);
                        map.put("file" + i + "\"; filename=\"" + f.getName(), file);
                        file = null;
                        f = null;
                    }

                    float price = 0;

                    if (!priceField.getText().toString().equals("")) {
                        price = Float.valueOf(priceField.getText().toString());
                    }

                    float ratio = prefs.getFloat("ratio", 0);

                    Post newPost = new Post("", userId, brandField.getText().toString(), modelField.getText().toString(), descriptionField.getText().toString(), price, currencySpinner.getText().toString(), latitude, longitude, ratio, "");

                    RestApi.Factory.getInstance().createPost(newPost, map).enqueue(new Callback<Boolean>() {
                        @Override
                        public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                            if (response.body()) {
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.remove("postId");
                                editor.remove("brand");
                                editor.remove("model");
                                editor.remove("description");
                                editor.remove("uriset");
                                editor.remove("price");
                                editor.remove("currency");
                                editor.remove("ratio");
                                editor.commit();

                                Intent intent = new Intent(getBaseContext(), SuccessActivity.class);
                                intent.putExtra("shareText", (brandField.getText().toString().equals("") ? "" : brandField.getText().toString() + " ") + modelField.getText().toString());
                                startActivity(intent);
                            } else {
                                Toast.makeText(getBaseContext(), R.string.connectionFailMsg, Toast.LENGTH_SHORT).show();
                                maskLayout.setVisibility(View.INVISIBLE);
                            }
                        }

                        @Override
                        public void onFailure(Call<Boolean> call, Throwable t) {
                            Toast.makeText(getBaseContext(), R.string.connectionFailMsg, Toast.LENGTH_SHORT).show();
                            maskLayout.setVisibility(View.INVISIBLE);
                        }
                    });
                } else {
                    // update post

                    List<String> uriListArray = new ArrayList<String>(Arrays.asList(uriSet.split("\\s*,\\s*")));
                    List<String> uriList = new ArrayList<String>(0);
                    for (String uri : uriListArray) {
                        if (!uri.equals("")) {
                            uriList.add(uri);
                        }
                    }
                    HashMap<String, RequestBody> map = new HashMap<>(uriList.size());
                    RequestBody file = null;
                    File f = null;
                    for (int i = 0; i < uriList.size(); i++) {
                        // except uris from web, only include from local storage
                        if (uriList.get(i).split("\\s*:\\s*")[0].equals("http")) {
                            continue;
                        }
                        String imageAbsolutePath = new File(uriList.get(i)).getAbsolutePath();
                        Bitmap bmp = decodeSampledBitmapFromUri(uriList.get(i));
                        Bitmap realBmp = null;
                        try {
                            realBmp = modifyOrientation(bmp, imageAbsolutePath);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        f = getCompressedFile(realBmp, i);
                        file = RequestBody.create(MEDIA_TYPE, f);
                        map.put("file" + i + "\"; filename=\"" + f.getName(), file);
                        file = null;
                        f = null;
                    }

                    float price = 0;

                    if (!priceField.getText().toString().equals("")) {
                        price = Float.valueOf(priceField.getText().toString());
                    }

                    float ratio = prefs.getFloat("ratio", 0);

                    Post newPost = new Post(postId, userId, brandField.getText().toString(), modelField.getText().toString(), descriptionField.getText().toString(), price, currencySpinner.getText().toString(), latitude, longitude, ratio, uriSet);

                    RestApi.Factory.getInstance().updatePost(newPost, map).enqueue(new Callback<Boolean>() {
                        @Override
                        public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                            if (response.body()) {
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.remove("postId");
                                editor.remove("brand");
                                editor.remove("model");
                                editor.remove("description");
                                editor.remove("uriset");
                                editor.remove("price");
                                editor.remove("currency");
                                editor.remove("ratio");
                                editor.commit();

                                Intent intent = new Intent(getBaseContext(), SuccessActivity.class);
                                intent.putExtra("shareText", (brandField.getText().toString().equals("") ? "" : brandField.getText().toString() + " ") + modelField.getText().toString());
                                startActivity(intent);
                            } else {
                                Toast.makeText(getBaseContext(), R.string.connectionFailMsg, Toast.LENGTH_SHORT).show();
                                maskLayout.setVisibility(View.INVISIBLE);
                            }
                        }

                        @Override
                        public void onFailure(Call<Boolean> call, Throwable t) {
                            Toast.makeText(getBaseContext(), R.string.connectionFailMsg, Toast.LENGTH_SHORT).show();
                            maskLayout.setVisibility(View.INVISIBLE);
                        }
                    });
                }
            }
        });
    }

    public boolean isValidPrice( String input )
    {
        if (input.equals("")) {
            return true;
        }
        try
        {
            int num = Integer.parseInt( input );
            if (num < 0)
                return false;
            return true;
        }
        catch( Exception ex)
        {
            return false;
        }
    }

    private void populatePhotoLayouts() {

        size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        int deviceWidth = size.x;

        scale = getResources().getDisplayMetrics().density;
        padding = (int) (20 * scale + 0.5f);

        picturesLayout = (GridLayout) findViewById(R.id.picturesLayout);
        postCoverLayout = (RelativeLayout) findViewById(R.id.postCoverLayout);

        String uriSet = prefs.getString("uriset", "");
        uriList = new ArrayList<String>(Arrays.asList(uriSet.split("\\s*,\\s*")));

        final int firstHeight = (deviceWidth - 4 * padding)/3 + padding * 2;
        final int secondHeight = (deviceWidth - 4 * padding)/3 * 2 + padding * 3;

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                (uriList.size() < 3)? firstHeight : secondHeight
        );
        picturesLayout.setLayoutParams(layoutParams);
        picturesLayout.setPadding(padding, padding, 0, 0);

        postCoverLayout.setLayoutParams(new ScrollView.LayoutParams(
                ScrollView.LayoutParams.MATCH_PARENT,
                size.y - (int) (80 * scale + 0.5f)
        ));

        for (int num = 0; num < uriList.size(); num++) {
            int marginRight = padding;
            int marginBottom = padding;
            final PhotoLayout photoLayout = new PhotoLayout(this, (deviceWidth - 4 * padding)/3, (deviceWidth - 4 * padding)/3, marginRight, marginBottom, num, uriList.get(num));
            final int imageNumber = num;
            photoLayout.getChildAt(1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    uriList.remove(v.getTag());
                    String uriUpdatedString = "";
                    for (int i = 0; i < uriList.size(); i++) {
                        if (i == 0) uriUpdatedString += uriList.get(i);
                        else uriUpdatedString += ("," + uriList.get(i));
                    }
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("uriset", uriUpdatedString);
                    editor.commit();
                    picturesLayout.removeView((View) v.getParent());
                    picturesLayout.setLayoutParams(new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT,
                            (uriList.size() < 3) ? firstHeight : secondHeight
                    ));
                    if (uriList.size() == 5) {
                        addNewImageBtn();
                    }
                }
            });
            picturesLayout.addView(photoLayout);
        }

        // add plus view to go to camera
        if (uriList.size() < 6) {
            addNewImageBtn();
        }
    }

    private void addNewImageBtn() {
        ImageButton addBtn = new ImageButton(picturesLayout.getContext());
        addBtn.setScaleType(ImageButton.ScaleType.FIT_CENTER);
        addBtn.setImageResource(R.drawable.add_image);
        addBtn.setBackgroundResource(R.drawable.add_plus_background);
        addBtn.setPadding((size.x - 4 * padding) / 9, (size.x - 4 * padding) / 9, (size.x - 4 * padding) / 9, (size.x - 4 * padding) / 9);
        RelativeLayout.LayoutParams addBtnParams = new RelativeLayout.LayoutParams((size.x - 4 * padding) / 3, (size.x - 4 * padding) / 3);
        addBtnParams.setMargins(0, 0, 0, padding);
        addBtn.setLayoutParams(addBtnParams);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), CameraActivity.class);
                intent.putExtra("fromActivity", "post");
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("imageNumber", uriList.size());
                editor.putString("brand", brandField.getText().toString());
                editor.putString("model", modelField.getText().toString());
                editor.putString("description", descriptionField.getText().toString());
                editor.putInt("price", priceField.getText().toString().equals("") ? 0 : Integer.parseInt(priceField.getText().toString()));
                editor.putString("currency", currencySpinner.getText().toString());
                editor.commit();
                startActivity(intent);
            }
        });
        picturesLayout.addView(addBtn);
    }

    private void setupActionBar() {

        final LayoutInflater inflater = (LayoutInflater) this.getSupportActionBar().getThemedContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View customActionBarView = inflater.inflate(R.layout.custom_action_bar, null);
        customActionBarView.findViewById(R.id.title);
        TextView actionBarTitle = (TextView) customActionBarView.findViewById(R.id.actionBarTitle);

        String postId = prefs.getString("postId", "");
        actionBarTitle.setText(postId.equals("") ? R.string.createItem : R.string.updateItem);

        ImageButton closePostPageBtn = (ImageButton) customActionBarView.findViewById(R.id.closePostPage);

        closePostPageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                startActivity(intent);
            }
        });

        final ActionBar actionBar = this.getSupportActionBar();
        actionBar.setDisplayOptions(
                ActionBar.DISPLAY_SHOW_CUSTOM, ActionBar.DISPLAY_SHOW_CUSTOM |
                        ActionBar.DISPLAY_SHOW_HOME |
                        ActionBar.DISPLAY_SHOW_TITLE);

        actionBar.setCustomView(customActionBarView,
                new ActionBar.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                )
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_ACCESS_FINE_LOCATION_PERMISSION_RESULT) {
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(),
                        R.string.access_denied, Toast.LENGTH_SHORT).show();
            }
        } else {
            tracker = new GPSTracker(PostActivity.this);

            if (tracker.isCanGetLocation()) {
                latitude = tracker.getLatitude();
                longitude = tracker.getLongitude();
            }
        }
    }
    public Bitmap decodeSampledBitmapFromUri(String imageUri) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageUri, options);
        File f = new File(imageUri);

        long maxSize = 200 * 1024;
        long fileSize = f.length();

        float ratio = 1;

        if (fileSize > maxSize) {
            ratio = (float)maxSize / (float)fileSize;
        }

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, Math.round(options.outWidth*ratio), Math.round(options.outHeight*ratio));

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(imageUri, options);
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    private File getCompressedFile(Bitmap image, int index) {
        File pictureFile = getOutputMediaFile(index);
        if (pictureFile == null) {
            return null;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.JPEG, 95, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Toast.makeText(PostActivity.this, "File not found", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(PostActivity.this, "Error accessing file: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return pictureFile;
    }

    /** Create a File for saving an image or video */
    private File getOutputMediaFile(int index){
        createCompressionImageFolder();
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
        File mediaFile;
        String mImageName="Img_"+ timeStamp + "_" + index +".jpg";
        mediaFile = new File(mCompressionImageFolder.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private void createCompressionImageFolder() {
        File imageFile = Environment.getExternalStoragePublicDirectory("");
        File rootDirectory = new File(imageFile, "CultishApp");
        if(!rootDirectory.exists()) {
            rootDirectory.mkdir();
        }
        File mediaFolder = new File(rootDirectory, "Media");
        if(!mediaFolder.exists()) {
            mediaFolder.mkdir();
        }
        mCompressionImageFolder = new File(mediaFolder, "Images_compression");
        if (!mCompressionImageFolder.exists()) {
            mCompressionImageFolder.mkdir();
        }
    }

    public Bitmap modifyOrientation(Bitmap bitmap, String image_absolute_path) throws IOException {
        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            default:
                return bitmap;
        }
    }

    public Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
}