package com.salismail.cultishexchange;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.salismail.cultishexchange.clientapi.RestApi;
import com.salismail.cultishexchange.clientapi.responseData.models.PostDetail;
import com.salismail.cultishexchange.ui.activity.BaseActivity;
import com.salismail.cultishexchange.ui.activity.DialogsActivity;
import com.salismail.cultishexchange.utils.Consts;
import com.salismail.cultishexchange.utils.SharedPreferencesUtil;
import com.salismail.cultishexchange.utils.chat.ChatHelper;

import com.salismail.cultishexchange.ui.activity.ChatActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostDetailActivity extends BaseActivity {

    public static final String EXTRA_QB_USERS = "qb_users";
    public static final int MINIMUM_CHAT_OCCUPANTS_SIZE = 2;
    private static final long CLICK_DELAY = TimeUnit.SECONDS.toMillis(2);

    private static final String EXTRA_QB_DIALOG = "qb_dialog";
    private static final int REQUEST_MARK_READ = 165;

    Point size;

    Context context;

    RelativeLayout maskLoading;
    ImageButton backToHome;

    TextView postTitle;
    TextView distValue;
    TextView postPrice;
    TextView userName;
    TextView viewNumber;
    TextView descView;

    ImageView postImage;
    ImageView userAvatar;

    com.rey.material.widget.RelativeLayout favoriteBtn;
    com.rey.material.widget.RelativeLayout savedBtn;
    ImageView savedIcon;
    ImageButton editPostBtn;
    ImageButton socialSharingBtn;

    double distance;
    GoogleMap googleMap;
    MapFragment mapView;

    SharedPreferences prefs;
    Set<String> liked;
    Set<String> saved;
    String userId;
    String currentPostId;
    int favorites;

    RelativeLayout imageSwiperLayout;
    ViewPager imageSwiperPager;

    FloatingActionButton chatButton;

    Boolean isSold = false;
    Boolean isFlag = false;

    PostDetail detail;
    LinearLayout priceLayout;
    LinearLayout pageIndicatorView;

    Boolean isAlreadyLiked = false;
    Boolean isAlreadySaved = false;

    Handler handler = new Handler();

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);

        // change status bar color
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(Color.BLACK);

        context = getBaseContext();

        size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);

        maskLoading = (RelativeLayout) findViewById(R.id.postViewLoading);
        chatButton = (FloatingActionButton) findViewById(R.id.goToChat);
        backToHome = (ImageButton) findViewById(R.id.backToHomePage);

        postTitle = (TextView) findViewById(R.id.postTitle);
        distValue = (TextView) findViewById(R.id.distValuePostView);
        postPrice = (TextView) findViewById(R.id.pricePostView);
        userName = (TextView) findViewById(R.id.userName);
        viewNumber = (TextView) findViewById(R.id.viewNum);
        descView = (TextView) findViewById(R.id.descriptionView);

        postImage = (ImageView) findViewById(R.id.imageSwiper);
        userAvatar = (ImageView) findViewById(R.id.userAvatar);

        favoriteBtn = (com.rey.material.widget.RelativeLayout) findViewById(R.id.viewCircle);
        savedBtn = (com.rey.material.widget.RelativeLayout) findViewById(R.id.favoriteCircle);
        savedIcon = (ImageView) findViewById(R.id.favoriteIcon);
        editPostBtn = (ImageButton) findViewById(R.id.editPost);
        socialSharingBtn = (ImageButton) findViewById(R.id.socialSharing);

        priceLayout = (LinearLayout) findViewById(R.id.priceLayout);
        imageSwiperLayout = (RelativeLayout) findViewById(R.id.imageSwiperLayout);
        imageSwiperLayout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, size.x));

        imageSwiperPager = (ViewPager) findViewById(R.id.imageSwiperPager);
        pageIndicatorView = (LinearLayout) findViewById(R.id.pageIndicator);

        prefs = getSharedPreferences("accountInfo", Context.MODE_PRIVATE);
        liked = prefs.getStringSet("liked", new HashSet<String>());
        saved = prefs.getStringSet("saved", new HashSet<String>());
        userId = prefs.getString("id", "");

        mapView = (MapFragment) getFragmentManager().findFragmentById(R.id.userLocationMap);
        mapView.getView().getLayoutParams().height = size.x/2;
        googleMap = mapView.getMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);


        if (getIntent().hasExtra("postId")) {
            String postId = getIntent().getStringExtra("postId");

            RestApi.Factory.getInstance().getPostById(postId).enqueue(new Callback<PostDetail>() {
                @Override
                public void onResponse(Call<PostDetail> call, Response<PostDetail> response) {
                    detail = response.body();

                    final List<String> imageList = new ArrayList<String>(Arrays.asList(detail.getImages().split("\\s*,\\s*")));

                    if (userId.equals(detail.getUserId())) {
                        chatButton.setVisibility(View.INVISIBLE);
                    }

                    currentPostId = detail.getId();

                    if (getIntent().hasExtra("distance")) {
                        distance = getIntent().getDoubleExtra("distance", 0);
                    }

                    isSold = detail.getSold();
                    isFlag = detail.getFlag();

                    if (detail.getSold()) {
                        priceLayout.setBackgroundResource(R.drawable.price_sold_background);
                        postPrice.setText(R.string.set_sold_lowercase);
                    }

                    if (imageList.size() == 1) {
                        postImage.setVisibility(View.VISIBLE);
                        Glide.with(getBaseContext()).load(imageList.get(0)).override(size.x, size.x).centerCrop().into(postImage);
                    } else {
                        imageSwiperPager.setVisibility(View.VISIBLE);
                        pageIndicatorView.setVisibility(View.VISIBLE);
                        ImageAdapter imageAdapter = new ImageAdapter(PostDetailActivity.this, imageList, size.x);
                        imageSwiperPager.setAdapter(imageAdapter);

                        for (int i = 0; i < imageList.size(); i++) {
                            final RelativeLayout pageIndicator = new RelativeLayout(PostDetailActivity.this);
                            float scale = getResources().getDisplayMetrics().density;
                            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) (15 * scale + 0.5f), (int) (15 * scale + 0.5f));
                            pageIndicator.setLayoutParams(params);
                            pageIndicator.setBackgroundResource(R.drawable.page_indicator_no_select);
                            if (i == 0) {
                                pageIndicator.setBackgroundResource(R.drawable.page_indicator_select);
                            }
                            pageIndicator.setTag(i);

                            pageIndicator.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    int position = (int) v.getTag();
                                    imageSwiperPager.setCurrentItem(position);

                                    for (int j = 0; j < imageList.size(); j++)
                                        pageIndicatorView.getChildAt(j).setBackgroundResource(R.drawable.page_indicator_no_select);
                                    v.setBackgroundResource(R.drawable.page_indicator_select);
                                }
                            });

                            pageIndicatorView.addView(pageIndicator);
                        }

                        imageSwiperPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                            // This method will be invoked when a new page becomes selected.
                            @Override
                            public void onPageSelected(int position) {
                                for (int j = 0; j < imageList.size(); j++) {
                                    pageIndicatorView.getChildAt(j).setBackgroundResource(R.drawable.page_indicator_no_select);
                                }
                                pageIndicatorView.getChildAt(position).setBackgroundResource(R.drawable.page_indicator_select);
                            }

                            // This method will be invoked when the current page is scrolled
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                // Code goes here
                            }

                            // Called when the scroll state changes:
                            // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
                            @Override
                            public void onPageScrollStateChanged(int state) {
                                // Code goes here
                            }
                        });
                    }

                    if (detail.getUserInfo().getPictureUrl() != null) {
                        Glide.with(getBaseContext()).load(detail.getUserInfo().getPictureUrl()).into(userAvatar);
                    }

                    if (liked.contains(detail.getId())) {
                        isAlreadyLiked = true;
                    }

                    if (saved.contains(detail.getId())) {
                        isAlreadySaved = true;
                        savedIcon.setImageResource(R.drawable.heart);
                    }

                    if (userId.equals(detail.getUserInfo().getId())) {
                        editPostBtn.setVisibility(View.VISIBLE);
                    }

                    userName.setText(detail.getUserInfo().getName());
                    favorites = detail.getFavorites();
                    viewNumber.setText(favorites + "");
                    if (!detail.getDescription().equals("")) {
                        descView.setText(detail.getDescription());
                    }

                    String brandTitle = detail.getBrand().equals("")? "" : detail.getBrand() + " ";
                    String modelTitle = detail.getModel();

                    postTitle.setText(brandTitle + modelTitle);
                    String distStr = Math.round(distance) + " m";
                    if (distance > 1000) {
                        distStr = Math.round(distance / 1000) + " km";
                    }
                    distValue.setText(distStr);

                    if (detail.getPrice() != 0 && !detail.getSold()) {
                        postPrice.setText(detail.getCurrency() + " " + detail.getPrice());
                    }

                    backToHome.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                            startActivity(intent);
                        }
                    });

                    final LatLng position = new LatLng(detail.getLocation().getCoordinates().get(1) , detail.getLocation().getCoordinates().get(0));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 17));
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);

                    googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {
                            double myLat = getIntent().getDoubleExtra("myLat", 0);
                            double myLng = getIntent().getDoubleExtra("myLng", 0);

                            Intent intent = new Intent(getBaseContext(), MapActivity.class);
                            intent.putExtra("myLat", myLat);
                            intent.putExtra("myLng", myLng);
                            intent.putExtra("sellerLat", detail.getLocation().getCoordinates().get(1));
                            intent.putExtra("sellerLng", detail.getLocation().getCoordinates().get(0));
                            startActivity(intent);
                        }
                    });

                    if (!userId.equals(detail.getUserId())) {
                        chatButton.setVisibility(View.VISIBLE);
                    }
                    maskLoading.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onFailure(Call<PostDetail> call, Throwable t) {
                    Toast.makeText(getBaseContext(), R.string.connectionFailMsg, Toast.LENGTH_SHORT).show();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                            startActivity(intent);
                        }
                    }, 400);
                }
            });

        } else {
            Toast.makeText(getBaseContext(), "Can't find post ID to show!", Toast.LENGTH_SHORT).show();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                    startActivity(intent);
                }
            }, 400);
        }

        editPostBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = getSharedPreferences("postImages", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("postId", detail.getId());
                editor.putString("brand", detail.getBrand());
                editor.putString("model", detail.getModel());
                editor.putString("description", detail.getDescription());
                editor.putString("uriset", detail.getImages());
                editor.putInt("price", detail.getPrice());
                editor.putString("currency", detail.getCurrency());
                editor.putFloat("ratio", detail.getRatio());
                editor.commit();

                Intent intent = new Intent(getBaseContext(), PostActivity.class);
                startActivity(intent);
            }
        });

        savedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Set<String> oldSaved = saved;
                if (!isAlreadySaved) {
                    saved.add(currentPostId);
                } else {
                    saved.remove(currentPostId);
                }
                List<String> savedList = new ArrayList<String>(saved);

                String updatedSavedStr = "";
                for (int i = 0; i < savedList.size(); i++) {
                    if (savedList.get(i).equals("")) continue;
                    if (updatedSavedStr.equals("")) updatedSavedStr += savedList.get(i);
                    else updatedSavedStr+= ("," + savedList.get(i));
                }

                RestApi.Factory.getInstance().updateSavedData(userId, updatedSavedStr).enqueue(new Callback<Boolean>() {
                    @Override
                    public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                        if (response.body()) {
                            Toast.makeText(PostDetailActivity.this, isAlreadySaved ? R.string.removed_success_msg : R.string.saved_success_msg, Toast.LENGTH_SHORT).show();
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putStringSet("saved", saved);
                            editor.commit();
                            savedIcon.setImageResource(isAlreadySaved ? R.drawable.heart_blank : R.drawable.heart);
                            isAlreadySaved = !isAlreadySaved;
                        } else {
                            Toast.makeText(PostDetailActivity.this, isAlreadySaved ? R.string.removed_failure_msg : R.string.saved_failure_msg, Toast.LENGTH_SHORT).show();
                            saved = oldSaved;
                        }
                    }

                    @Override
                    public void onFailure(Call<Boolean> call, Throwable t) {
                        Toast.makeText(PostDetailActivity.this, R.string.connectionFailMsg, Toast.LENGTH_SHORT).show();
                        saved = oldSaved;
                    }
                });
            }
        });

        favoriteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAlreadyLiked) {
                    Toast.makeText(PostDetailActivity.this, R.string.already_liked, Toast.LENGTH_SHORT).show();
                    return;
                }

                final Set<String> oldLiked = liked;
                liked.add(currentPostId);
                List<String> likedList = new ArrayList<String>(liked);

                String updatedLikedStr = "";
                for (int i = 0; i < likedList.size(); i++) {
                    if (likedList.get(i).equals("")) continue;
                    if (updatedLikedStr.equals("")) updatedLikedStr += likedList.get(i);
                    else updatedLikedStr += ("," + likedList.get(i));
                }

                RestApi.Factory.getInstance().updateLikedData(userId, updatedLikedStr, currentPostId).enqueue(new Callback<Boolean>() {
                    @Override
                    public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                        if (response.body()) {
                            Toast.makeText(PostDetailActivity.this, "success!", Toast.LENGTH_SHORT).show();
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putStringSet("liked", liked);
                            editor.commit();
                            favorites++;
                            viewNumber.setText(favorites + "");
                            isAlreadyLiked = !isAlreadyLiked;
                        } else {
                            Toast.makeText(PostDetailActivity.this, "failure!", Toast.LENGTH_SHORT).show();
                            liked = oldLiked;
                        }
                    }

                    @Override
                    public void onFailure(Call<Boolean> call, Throwable t) {
                        Toast.makeText(PostDetailActivity.this, R.string.connectionFailMsg, Toast.LENGTH_SHORT).show();
                        liked = oldLiked;
                    }
                });
            }
        });

        final ImageButton settingBtn = (ImageButton) findViewById(R.id.settingBtn);
        settingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(PostDetailActivity.this, settingBtn);
                // detect if user is seller or not
                Boolean isSeller = userId.equals(detail.getUserInfo().getId());
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(isSeller ? R.menu.seller_setting : R.menu.buyer_setting, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        int id = item.getItemId();

                        //noinspection SimplifiableIfStatement
                        if (id == R.id.delete_post) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(PostDetailActivity.this);
                            alertDialog.setTitle(R.string.delete_post);
                            alertDialog.setMessage(R.string.delete_post_confirm)
                                    .setCancelable(false)
                                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (!userId.equals(detail.getUserId())) {
                                                chatButton.setVisibility(View.INVISIBLE);
                                            }
                                            maskLoading.setVisibility(View.VISIBLE);

                                            final DialogInterface dialogExt = dialog;

                                            RestApi.Factory.getInstance().deletePost(detail.getId()).enqueue(new Callback<Boolean>() {
                                                @Override
                                                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                                    Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                                                    startActivity(intent);
                                                }

                                                @Override
                                                public void onFailure(Call<Boolean> call, Throwable t) {
                                                    if (!userId.equals(detail.getUserId())) {
                                                        chatButton.setVisibility(View.VISIBLE);
                                                    }
                                                    maskLoading.setVisibility(View.INVISIBLE);
                                                    Toast.makeText(getBaseContext(), R.string.connectionFailMsg, Toast.LENGTH_SHORT).show();
                                                    dialogExt.cancel();
                                                }
                                            });
                                        }
                                    })
                                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog dialog = alertDialog.create();
                            dialog.show();

                            return true;
                        } else if (id == R.id.set_sold) {

                            if (isSold) {
                                Toast.makeText(getBaseContext(), R.string.already_sold, Toast.LENGTH_SHORT).show();
                                return true;
                            }

                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(PostDetailActivity.this);
                            alertDialog.setTitle(R.string.set_sold);
                            alertDialog.setMessage(R.string.sold_confirm)
                                    .setCancelable(false)
                                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            if (!userId.equals(detail.getUserId())) {
                                                chatButton.setVisibility(View.INVISIBLE);
                                            }
                                            maskLoading.setVisibility(View.VISIBLE);

                                            final DialogInterface dialogExt = dialog;

                                            RestApi.Factory.getInstance().setSold(detail.getId()).enqueue(new Callback<Boolean>() {
                                                @Override
                                                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                                    if (!userId.equals(detail.getUserId())) {
                                                        chatButton.setVisibility(View.VISIBLE);
                                                    }
                                                    maskLoading.setVisibility(View.INVISIBLE);
                                                    priceLayout.setBackgroundResource(R.drawable.price_sold_background);
                                                    postPrice.setText(R.string.set_sold_lowercase);
                                                    isSold = !isSold;
                                                }

                                                @Override
                                                public void onFailure(Call<Boolean> call, Throwable t) {
                                                    if (!userId.equals(detail.getUserId())) {
                                                        chatButton.setVisibility(View.VISIBLE);
                                                    }
                                                    maskLoading.setVisibility(View.INVISIBLE);
                                                    Toast.makeText(getBaseContext(), R.string.connectionFailMsg, Toast.LENGTH_SHORT).show();
                                                    dialogExt.cancel();
                                                }
                                            });
                                        }
                                    })
                                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog dialog = alertDialog.create();
                            dialog.show();

                            return true;
                        } else if (id == R.id.flag_this) {

                            if (isFlag) {
                                Toast.makeText(getBaseContext(), R.string.already_flag, Toast.LENGTH_SHORT).show();
                                return true;
                            }

                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(PostDetailActivity.this);
                            alertDialog.setTitle(R.string.flag_this);
                            alertDialog.setMessage(R.string.flag_this_confirm)
                                    .setCancelable(false)
                                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            if (!userId.equals(detail.getUserId())) {
                                                chatButton.setVisibility(View.INVISIBLE);
                                            }
                                            maskLoading.setVisibility(View.VISIBLE);

                                            final DialogInterface dialogExt = dialog;

                                            RestApi.Factory.getInstance().setFlag(detail.getId()).enqueue(new Callback<Boolean>() {
                                                @Override
                                                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                                    if (!userId.equals(detail.getUserId())) {
                                                        chatButton.setVisibility(View.VISIBLE);
                                                    }
                                                    maskLoading.setVisibility(View.INVISIBLE);
                                                    isFlag = !isFlag;
                                                }

                                                @Override
                                                public void onFailure(Call<Boolean> call, Throwable t) {
                                                    if (!userId.equals(detail.getUserId())) {
                                                        chatButton.setVisibility(View.VISIBLE);
                                                    }
                                                    maskLoading.setVisibility(View.INVISIBLE);
                                                    Toast.makeText(getBaseContext(), R.string.connectionFailMsg, Toast.LENGTH_SHORT).show();
                                                    dialogExt.cancel();
                                                }
                                            });
                                        }
                                    })
                                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog dialog = alertDialog.create();
                            dialog.show();

                            return true;
                        }

                        return true;
                    }
                });

                popup.show();

            }
        });

        socialSharingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popup = new PopupMenu(PostDetailActivity.this, socialSharingBtn);
                popup.getMenuInflater().inflate(R.menu.select_social, popup.getMenu());

                final PackageManager pm = getPackageManager();

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        int id = item.getItemId();

                        //noinspection SimplifiableIfStatement
                        if (id == R.id.emaill) {
                            Intent intent = new Intent(Intent.ACTION_SEND);

                            intent.setType("message/rfc822");
                            String text = (detail.getBrand().equals("")? "" : detail.getBrand() + " ") + detail.getModel();

                            intent.putExtra(Intent.EXTRA_TEXT, text);

                            startActivity(Intent.createChooser(intent, "Share with"));

                            return true;
                        } else if (id == R.id.whatsapp) {
                            try {
                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.setType("text/plain");
                                String text = (detail.getBrand().equals("")? "" : detail.getBrand() + " ") + detail.getModel();

                                pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                                intent.setPackage("com.whatsapp");

                                intent.putExtra(Intent.EXTRA_TEXT, text);
                                startActivity(Intent.createChooser(intent, "Share with"));

                            } catch (PackageManager.NameNotFoundException e) {
                                Toast.makeText(PostDetailActivity.this, R.string.package_whatsapp_error, Toast.LENGTH_SHORT).show();
                            }

                            return true;
                        } else if (id == R.id.wechat) {
                            try {
                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.setType("text/plain");
                                String text = (detail.getBrand().equals("")? "" : detail.getBrand() + " ") + detail.getModel();

                                pm.getPackageInfo("com.tencent.mm", PackageManager.GET_META_DATA);
                                intent.setPackage("com.tencent.mm");

                                intent.putExtra(Intent.EXTRA_TEXT, text);
                                startActivity(Intent.createChooser(intent, "Share with"));

                            } catch (PackageManager.NameNotFoundException e) {
                                Toast.makeText(PostDetailActivity.this, R.string.package_wechat_error, Toast.LENGTH_SHORT).show();
                            }

                            return true;
                        } else if (id == R.id.line) {
                            try {
                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.setType("text/plain");
                                String text = (detail.getBrand().equals("")? "" : detail.getBrand() + " ") + detail.getModel();

                                pm.getPackageInfo("jp.naver.line.android", PackageManager.GET_META_DATA);
                                intent.setPackage("jp.naver.line.android");

                                intent.putExtra(Intent.EXTRA_TEXT, text);
                                startActivity(Intent.createChooser(intent, "Share with"));

                            } catch (PackageManager.NameNotFoundException e) {
                                Toast.makeText(PostDetailActivity.this, R.string.package_line_error, Toast.LENGTH_SHORT).show();
                            }

                            return true;
                        }

                        return true;
                    }
                });

                popup.show();
            }
        });

        chatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!userId.equals(detail.getUserId())) {
                    chatButton.setVisibility(View.INVISIBLE);
                }
                maskLoading.setVisibility(View.VISIBLE);

                QBUsers.getUserByLogin(detail.getUserInfo().getId(), new QBEntityCallback<QBUser>() {
                    @Override
                    public void onSuccess(QBUser qbUser, Bundle bundle) {
                        qbUser.setPassword(Consts.QB_USERS_PASSWORD);

                        QBUser myUser = SharedPreferencesUtil.getQbUser();

                        ArrayList<QBUser> occupantUsers = new ArrayList<QBUser>(Arrays.asList(myUser, qbUser));

                        Intent result = new Intent();
                        result.putExtra(EXTRA_QB_USERS, occupantUsers);

                        ChatHelper.getInstance().createDialogWithSelectedUsers(occupantUsers,
                                new QBEntityCallback<QBDialog>() {
                                    @Override
                                    public void onSuccess(QBDialog dialog, Bundle args) {

                                        SharedPreferences chatSeletedUser = getSharedPreferences("chat_selected_user", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor chatUserEditor = chatSeletedUser.edit();
                                        chatUserEditor.putString("userEmail", detail.getUserInfo().getEmail());
                                        List<String> nameList = new ArrayList<String>(Arrays.asList(detail.getUserInfo().getName().split("\\s* \\s*")));
                                        chatUserEditor.putString("userFirstName", nameList.get(0));
                                        chatUserEditor.putString("userAvatar", detail.getUserInfo().getPictureUrl());
                                        chatUserEditor.putBoolean("isBuyer", false);
                                        chatUserEditor.putString("brandModel", (detail.getBrand().equals("")? "" : detail.getBrand() + " ") + detail.getModel());
                                        chatUserEditor.putInt("price", detail.getPrice());
                                        chatUserEditor.putString("currency", detail.getCurrency());
                                        List<String> imageList = new ArrayList<String>(Arrays.asList(detail.getImages().split("\\s*,\\s*")));
                                        chatUserEditor.putString("postImgUrl", imageList.get(0));
                                        chatUserEditor.commit();

                                        ChatActivity.startForResult(PostDetailActivity.this, REQUEST_MARK_READ, dialog);
                                        finish();
                                    }

                                    @Override
                                    public void onError(QBResponseException e) {
                                        Toast.makeText(getBaseContext(), "Can not create chat dialog.", Toast.LENGTH_LONG).show();

                                        if (!userId.equals(detail.getUserId())) {
                                            chatButton.setVisibility(View.VISIBLE);
                                        }
                                        maskLoading.setVisibility(View.INVISIBLE);
                                    }
                                }
                        );
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        Toast.makeText(getBaseContext(), "Can not get target's chat info.", Toast.LENGTH_LONG).show();

                        if (!userId.equals(detail.getUserId())) {
                            chatButton.setVisibility(View.VISIBLE);
                        }
                        maskLoading.setVisibility(View.INVISIBLE);
                    }
                });
            }
        });
    }

    @Override
    public void onSessionCreated(boolean success) {
        if (success) {
            // do nothing
        }
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }
}